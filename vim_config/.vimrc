" =============================================================================
" ======================        GENERAL SETTINGS         ======================
" =============================================================================

" ----------------------          Looks & Style          ----------------------
set laststatus=2
set t_Co=256
set number

" ----------------------         Moves and Navigation     ----------------------
"So I can move around in insert
inoremap <C-k> <C-o>gk
inoremap <C-h> <Left>
inoremap <C-l> <Right>
inoremap <C-j> <C-o>gj



" ----------------------             Pyhon mode          ----------------------
let g:pymode = 1
let g:pymode_trim_whitespaces = 1
let g:pymode_options = 1
let g:pymode_ident = 1
let g:pymode_doc = 1
let g:pymode_doc_bind = 'K'
let g:pymode_rope = 1
let g:pymode_rope_completation = 1
let g:pymode_rope_complete_on_dot = 1
let g:pymode_rope_show_doc_bind = '<C-c>d'
let g:pymode_rope_rename_bind = '<C-c>rr'
let g:pymode_rope_autoimport = 1
let g:pymode_rope_goto_definition_cmd = 'new'
let g:pymode_rope_goto_definition_bind = '<C-c>g'
let g:pymode_syntax = 1
let g:pymode_syntax_all = 1
set completeopt=menuone,noinsert
let g:SimplyFold_fold_docstring = 0

